const bankAccount = {
	name: "Brandon Cruz",
	accountNo: "123-456",
	accountBal: 5000,
	showCustomer: function() {
		return "Customer Name: " + bankAccount.name;
	},
	showAccountNumber: function() {
		return "Customer Account Number: " + bankAccount.accountNo;
	},
	showCustomerDetails: function() {
		return "Customer Name: " + bankAccount.name + ", " + "Customer Account Number:" + bankAccount.accountNo + ", " + "Balance:" + bankAccount.accountBal;
	},
	Deposit: function(amount) {
		return bankAccount.accountBal += amount;
	},
	Withdraw: function(amount) {
		return bankAccount.accountBal -= amount;
	},

};
