const student = {
	age: 22,
	studentNumber: "2014-15233",
	college: "Science",
	course: "BS Physics",
	department: "Physics",
	isSingle: false,
	motto: "Time is gold",
	name: "Brandon",
	address: {
		stname: "Mahogany St.",
		brgy: "Poblacion",
		city: "Makati City",
		zipCode: "12345",
	},
	showAge: function(){
		console.log (student.age);
		return student.age;
	},
	addAge: function(){
			//add 1 to the age property of student
			student.age += 1;
			return "Successfully added age!";
	}
};

const student2 = {
	name: "Brenda"
}

student.gender = "Male";
student.isSingle = false;
delete student.studentNumber;