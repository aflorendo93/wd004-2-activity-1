const num1 = 2;
const num2 = 4;

const sum1 = num1 + num2;
console.log(sum1);

const num3 = 5;
const num4 = 3;

const sum2 = num3 + num4;
console.log(sum2)

const num5 = 11;
const num6 = 3;

const sum3 = num5 + num6;
console.log(sum3);

// DRY Principle
// Don't Repeat Yourself
//
//	Functions
// subprogram that is designed to a particular task
//
// We need a function that will add two numbers
// syntax:
function nameOfTheFunction(){
	//task or what your function should do
}
//
// function names/ variables should be in camel case
// remember, just like your variables, function names should be descriptive
// parameters - these are the values that the function needs in order to do its task

// If the function does not have a return value, it will return an undefined data
function getSum(num1, num2) {
	const sum = num1 + num2;
	console.log(sum)
	return sum;
}

// function call
//Arguments -  these are the values that will send to the function
getSum(2,3); //2 is num1, 3 is num2
// getSum(4,5);
// getSum(9,8);

const ageSum = getSum(15, 18)
console.log(ageSum)