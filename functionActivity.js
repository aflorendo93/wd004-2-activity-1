function getSum(num1, num2) {
	const sum = num1 + num2;
	return sum;
}

function getDifference(num1, num2) {
	const difference = num1 - num2;
	return difference;
}

function getProduct(num1, num2) {
	const product = num1 * num2;
	return product;
}

function getQuotient(num1, num2) {
	const quotient = num1 / num2;
	return quotient;
}

function getCylinderVolume(radius, height) {
	const rad1 = radius * radius;
	const pipart = 3.14 * rad1;
	const cylindervolume = pipart * height;
	return cylindervolume;
}

function getTrapezoidArea(base1, base2, height) {
	const base = base1 + base2;
	const basetotal = base / 2;
	const trapezoidarea = basetotal * height;
	return trapezoidarea;
}

function getTotalAmount(itemcost, quantity, discount, tax) {
	const itemxquantity = itemcost * quantity;
	const discounta = discount/100;
	const totaldiscount = itemxquantity * discounta;
	const totaldiscountedprice = itemxquantity - totaldiscount;
	const taxa = tax/100;
	const totaltaxed = totaldiscountedprice * taxa;
	const totalamount = totaldiscountedprice + totaltaxed; 
	return totalamount;
}

function getTotalSalary(monthsal, totalworkdays, daysabsent, minuteslate, tax) {
	const dailysal = monthsal / totalworkdays;
	const minutededuct = minuteslate / 1440;
	const deduct = daysabsent + minutededuct;
	const daysnagin = totalworkdays - deduct;
	const notaxsal = dailysal * daysnagin;
	const taxa = tax/100;
	const taxsal = taxa * notaxsal;
	const totalsalary = notaxsal - taxsal; 
	return totalsalary;
}
